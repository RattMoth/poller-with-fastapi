CREATE TABLE auto_vo(
    id SERIAL NOT NULL PRIMARY KEY,
    vin VARCHAR(20) NOT NULL UNIQUE,
    year INTEGER NOT NULL,
    make VARCHAR(100) NOT NULL,
    model VARCHAR(100) NOT NULL,
    color VARCHAR(100) NOT NULL
);

INSERT INTO auto_vo (vin, year, make, model, color)
VALUES ('zyx987', 1000, 'Thor', 'rock waggon', 'wood');



