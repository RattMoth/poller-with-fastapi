import os
from psycopg_pool import ConnectionPool

print("environ info:", os.environ["DATABASE_URL"])
pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])

print("This is the pool variable", pool)


class AutoVOQueries:
    def get_all_autos(self):
        with pool.connection() as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    """
                SELECT vin, year, make, model, color
                FROM auto_vo
                """
                )

                rows = cursor.fetchall()
                autos = []
                for row in rows:
                    auto = {
                        "vin": row[0],
                        "year": row[1],
                        "make": row[2],
                        "model": row[3],
                        "color": row[4],
                    }
                    autos.append(auto)
                return autos

    def create_autovo(self, auto):
        with pool.connection() as connection:
            with connection.cursor() as cursor:
                query = """
                INSERT INTO auto_vo (vin, year, make, model, color)
                VALUES (%s, %s, %s, %s, %s)
                ON CONFLICT (vin) DO NOTHING
                """
                cursor.execute(
                    query,
                    [
                        auto["vin"],
                        auto["year"],
                        auto["make"],
                        auto["model"],
                        auto["color"],
                    ],
                )
